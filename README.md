# Intern-Project

This repository contains my Python code for the project I had to implement during my intern.
The purpose of this project was to extract spatial and data information from images that depict supermarket shelves filled with products. 

Due to limited dataset, we recongized objects in 3 categories: 'skip' - 'altis' - 'omo'

It consists of 3 source code scripts:
1) binary_cnn.py
2) cnn_keras_train.py
3) product_detection_recognition.py

The first & second produce the appropriate models that are neccessary for the third to be executed.

The first one decides whether an object is a product or not.
The second trains a model to decide to which of the 3 classes an object belongs.
The third is the core script that executes the project and produces the results in an grid (as a visualization)


